import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import "firebase/storage";
import "firebase/messaging";

// Fill this in with your firebase details
var config = {
  apiKey: "AIzaSyBOsmajflre8ylA_H1NI82RHm0ojEokEGA",
  authDomain: "kl-liew-web.firebaseapp.com",
  projectId: "kl-liew-web",
  storageBucket: "kl-liew-web.appspot.com",
  messagingSenderId: "1018556230441",
  appId: "1:1018556230441:web:0f350d538e273d12f32538",
  measurementId: "G-S3V8VNNP8G"
};

!firebase.apps.length ? firebase.initializeApp(config) : "";

export const auth = firebase.auth();
export const persistencetype = firebase.auth.Auth.Persistence.LOCAL;

//Comment this out if not using local emulator
// firebase.firestore().settings({
//   host: "localhost:8080",
//   ssl: false,
// });

//Specify the link to firestoreDB collection that you will be using, feel free to change, add, delete.
export const firestoreDB = firebase.firestore().collection("users").doc("users");

export const storage = firebase.app().storage("gs://YOUR_BUCKET_HERE");
// export const messaging = firebase.messaging();
// // Set VAPID KEY
// messaging.usePublicVapidKey(
//   "BDYE2EYHdIp8qHjTKcJYPvO4PgaAH2pSruP55FOtNs5jWsgdeg7YK6OgJ0daSu21kN7aSzU19NRXRqC4bfITZYQ"
// );
export const GoogleProvider = new firebase.auth.GoogleAuthProvider()
export const FacebookProvider = new firebase.auth.FacebookAuthProvider()

export default firebase;
